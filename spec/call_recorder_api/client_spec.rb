require 'call_recorder_api/client'

module CallRecorderApi

    describe Client do
        context "When testing the Client class" do

            it "should call buyCreditsPost successfully", :skip => true do
                puts 'Run test for buy_credits...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.buy_credits(
                        BuyCreditsRequest.new(
                            api_key = apiKey,
                            amount = 100,
                            receipt = 'test',
                            product_id = 1,
                            device_type = 'ios',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call cloneFilePost successfully" do
                puts 'Run test for clone_file...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.clone_file(
                        CloneFileRequest.new(
                            api_key = apiKey,
                            id = fileId,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.code).to(eq('file_cloned'))
                expect(response.id).to(be >(0))
                puts('')

            end

            it "should call createFilePost successfully" do
                puts 'Run test for create_file...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.create_file(
                        CreateFileRequest.new(
                            api_key = apiKey,
                            file = 'spec/resources/audio.mp3',
                            data = CreateFileData.new(
                                name = 'test-file',
                                email = 'e@mail.com',
                                phone = '+16463742122',
                                l_name = 'l',
                                f_name = 'f',
                                notes = 'n',
                                tags = 't',
                                source = '0',
                                remind_days = '10',
                                remind_date = '2019-09-03T21:11:51.824121+03:00',
                            ),
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.id).to(be >(0))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call createFolderPost successfully" do
                puts 'Run test for create_folder...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.create_folder(
                        CreateFolderRequest.new(
                            api_key = apiKey,
                            name = 'test-folder',
                            pass = '12345',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.id).to(be >(0))
                expect(response.code).to(eq('folder_created'))
                puts('')

            end

            it "should call deleteFilesPost successfully" do
                puts 'Run test for delete_files...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.delete_files(
                        DeleteFilesRequest.new(
                            api_key = apiKey,
                            ids = [fileId],
                            action = 'remove_forever',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call deleteFolderPost successfully" do
                puts 'Run test for delete_folder...'
                client = Client.new
                response = provide_api_key_and_folder_id(client, -> (client, apiKey, folderId) {
                    client.delete_folder(
                        DeleteFolderRequest.new(
                            api_key = apiKey,
                            id = folderId,
                            move_to = '',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call deleteMetaFilesPost successfully", :skip => true do
                puts 'Run test for delete_meta_files...'
                client = Client.new
                response = provide_api_key_and_file_id_and_meta_file_id(client, -> (client, apiKey, fileId, metaFileId) {
                    client.delete_meta_files(
                        DeleteMetaFilesRequest.new(
                            api_key = apiKey,
                            ids = [metaFileId],
                            parent_id = fileId,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call getFilesPost successfully" do
                puts 'Run test for get_files...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_files(
                        GetFilesRequest.new(
                            api_key = apiKey,
                            page = 0,
                            folder_id = 0,
                            source = 'all',
                            pass = '12345',
                            reminder = false,
                            q = 'hello',
                            id = 0,
                            op = 'greater',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.credits).to(eq(0))
                expect(response.credits_trans).to(eq(0))
                expect(response.files).to(be_empty())
                puts('')

            end

            it "should call getFoldersPost successfully" do
                puts 'Run test for get_folders...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_folders(
                        GetFoldersRequest.new(
                            api_key = apiKey,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.folders).not_to(be_empty())
                puts('')

            end

            it "should call getLanguagesPost successfully" do
                puts 'Run test for get_languages...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_languages(
                        GetLanguagesRequest.new(
                            api_key = apiKey,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.languages).not_to(be_nil())
                puts('')

            end

            it "should call getMetaFilesPost successfully" do
                puts 'Run test for get_meta_files...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.get_meta_files(
                        GetMetaFilesRequest.new(
                            api_key = apiKey,
                            parent_id = fileId,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.meta_files).not_to(be_nil())
                puts('')

            end

            it "should call getMsgsPost successfully" do
                puts 'Run test for get_msgs...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_msgs(
                        GetMessagesRequest.new(
                            api_key = apiKey,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msgs).not_to(be_nil())
                puts('')

            end

            it "should call getPhonesPost successfully" do
                puts 'Run test for get_phones...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_phones(
                        GetPhonesRequest.new(
                            api_key = apiKey,
                        )
                    )
                })
                puts(response)
                puts('')

            end

            it "should call getProfilePost successfully" do
                puts 'Run test for get_profile...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_profile(
                        GetProfileRequest.new(
                            api_key = apiKey,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.code).not_to(be_empty())
                expect(response.profile).not_to(be_nil())
                expect(response.app).not_to(be_nil())
                expect(response.share_url).not_to(be_nil())
                expect(response.rate_url).not_to(be_nil())
                expect(response.credits).to(eq(0))
                expect(response.credits_trans).to(eq(0))
                puts('')

            end

            it "should call getSettingsPost successfully" do
                puts 'Run test for get_settings...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_settings(
                        GetSettingsRequest.new(
                            api_key = apiKey,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.app).not_to(be_nil())
                expect(response.credits).to(eq(0))
                expect(response.settings).not_to(be_nil())
                puts('')

            end

            it "should call getTranslationsPost successfully" do
                puts 'Run test for get_translations...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.get_translations(
                        GetTranslationsRequest.new(
                            api_key = apiKey,
                            language = 'en_US',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.translation).not_to(be_nil())
                puts('')

            end

            it "should call notifyUserCustomPost successfully", :skip => true do
                puts 'Run test for notify_user_custom...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.notify_user_custom(
                        NotifyUserRequest.new(
                            api_key = apiKey,
                            title = 'test-title',
                            body = 'test-body',
                            device_type = 'ios',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call recoverFilePost successfully", :skip => true do
                puts 'Run test for recover_file...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.recover_file(
                        RecoverFileRequest.new(
                            api_key = apiKey,
                            id = fileId,
                            folder_id = nil,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call registerPhonePost successfully" do
                puts 'Run test for register_phone...'
                client = Client.new
                response = client.register_phone(
                    RegisterPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                    )
                )
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.phone).not_to(be_nil())
                expect(response.code).not_to(be_empty())
                expect(response.msg).to(include('Verification Code Sent'))
                puts('')

            end

            it "should call updateDeviceTokenPost successfully" do
                puts 'Run test for update_device_token...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.update_device_token(
                        UpdateDeviceTokenRequest.new(
                            api_key = apiKey,
                            device_token = '871284c348e04a9cacab8aca6b2f3c9a',
                            device_type = 'ios',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call updateFilePost successfully" do
                puts 'Run test for update_file...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.update_file(
                        UpdateFileRequest.new(
                            api_key = apiKey,
                            id = fileId,
                            f_name = 'f',
                            l_name = 'l',
                            notes = 'n',
                            email = 'e@mail.ru',
                            phone = '+16463742122',
                            tags = 't',
                            folder_id = 0,
                            name = 'n',
                            remind_days = '10',
                            remind_date = '2019-09-03T21:11:51.824121+03:00',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                puts('')

            end

            it "should call updateFolderPost successfully" do
                puts 'Run test for update_folder...'
                client = Client.new
                response = provide_api_key_and_folder_id(client, -> (client, apiKey, folderId) {
                    client.update_folder(
                        UpdateFolderRequest.new(
                            api_key = apiKey,
                            id = folderId,
                            name = 'n',
                            pass = '12345',
                            is_private = true,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg).to(include('Folder Updated'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call updateOrderPost successfully" do
                puts 'Run test for update_order...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.update_order(
                        UpdateOrderRequest.new(
                            api_key = apiKey,
                            folders = [],
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg).to(include('Order Updated'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call updateProfileImgPost successfully" do
                puts 'Run test for update_profile_img...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.update_profile_img(
                        UpdateProfileImgRequest.new(
                            api_key = apiKey,
                            file = 'spec/resources/java.png',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg).to(include('Profile Picture Updated'))
                expect(response.code).not_to(be_empty())
                expect(response.file).not_to(be_nil())
                expect(response.path).not_to(be_nil())
                puts('')

            end

            it "should call updateProfilePost successfully" do
                puts 'Run test for update_profile...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.update_profile(
                        UpdateProfileRequest.new(
                            api_key = apiKey,
                            data = UpdateProfileRequestData.new(
                                f_name = 'f',
                                l_name = 'l',
                                email = 'e@mail.ru',
                                is_public = '',
                                language = '',
                            ),
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg).to(include('Profile Updated'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call updateSettingsPost successfully" do
                puts 'Run test for update_settings...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.update_settings(
                        UpdateSettingsRequest.new(
                            api_key = apiKey,
                            play_beep = 'no',
                            files_permission = 'private',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call updateStarPost successfully" do
                puts 'Run test for update_star...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.update_star(
                        UpdateStarRequest.new(
                            api_key = apiKey,
                            id = fileId,
                            star = true,
                            type = 'file',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call updateUserPost successfully" do
                puts 'Run test for update_user...'
                client = Client.new
                response = provide_api_key(client, -> (client, apiKey) {
                    client.update_user(
                        UpdateUserRequest.new(
                            api_key = apiKey,
                            app = 'rec',
                            time_zone = '10',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call uploadMetaFilePost successfully" do
                puts 'Run test for upload_meta_file...'
                client = Client.new
                response = provide_api_key_and_file_id(client, -> (client, apiKey, fileId) {
                    client.upload_meta_file(
                        UploadMetaFileRequest.new(
                            api_key = apiKey,
                            file = 'spec/resources/java.png',
                            name = 'test-meta',
                            parent_id = fileId,
                            id = 1,
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg.downcase()).to(include('success'))
                expect(response.id).to(be >(0))
                puts('')

            end

            it "should call verifyFolderPassPost successfully" do
                puts 'Run test for verify_folder_pass...'
                client = Client.new
                response = provide_api_key_and_folder_id(client, -> (client, apiKey, folderId) {
                    client.verify_folder_pass(
                        VerifyFolderPassRequest.new(
                            api_key = apiKey,
                            id = folderId,
                            pass = '12345',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.msg).to(include('Password is Correct'))
                expect(response.code).not_to(be_empty())
                puts('')

            end

            it "should call verifyPhonePost successfully" do
                puts 'Run test for verify_phone...'
                client = Client.new
                response = provide_code(client, -> (client, code) {
                    client.verify_phone(
                        VerifyPhoneRequest.new(
                            token = '55942ee3894f51000530894',
                            phone = '+16463742122',
                            code = code,
                            mcc = '300',
                            app = 'rec',
                            device_token = '871284c348e04a9cacab8aca6b2f3c9a',
                            device_id = '871284c348e04a9cacab8aca6b2f3c9a',
                            device_type = 'ios',
                            time_zone = '10',
                        )
                    )
                })
                puts(response)
                expect(response.status).to(eq('ok'))
                expect(response.phone).not_to(be_nil())
                expect(response.api_key).not_to(be_nil())
                expect(response.msg).to(include('Phone Verified'))
                puts('')

            end

            def provide_code(client, callback)
                register_phone_post_response = client.register_phone(
                    RegisterPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                    )
                )
                register_phone_post_response_code = register_phone_post_response.code
                code = register_phone_post_response_code
                callback.(client, code)
            end


            def provide_api_key(client, callback)
                register_phone_post_response = client.register_phone(
                    RegisterPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                    )
                )
                register_phone_post_response_code = register_phone_post_response.code
                verify_phone_post_response = client.verify_phone(
                    VerifyPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                        code = register_phone_post_response_code,
                        mcc = '300',
                        app = 'rec',
                        device_token = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_id = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_type = 'ios',
                        time_zone = '10',
                    )
                )
                verify_phone_post_response_api_key = verify_phone_post_response.api_key
                apiKey = verify_phone_post_response_api_key
                callback.(client, apiKey)
            end


            def provide_api_key_and_file_id(client, callback)
                register_phone_post_response = client.register_phone(
                    RegisterPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                    )
                )
                register_phone_post_response_code = register_phone_post_response.code
                verify_phone_post_response = client.verify_phone(
                    VerifyPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                        code = register_phone_post_response_code,
                        mcc = '300',
                        app = 'rec',
                        device_token = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_id = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_type = 'ios',
                        time_zone = '10',
                    )
                )
                verify_phone_post_response_api_key = verify_phone_post_response.api_key
                create_file_post_response = client.create_file(
                    CreateFileRequest.new(
                        api_key = verify_phone_post_response_api_key,
                        file = 'spec/resources/audio.mp3',
                        data = '',
                    )
                )
                create_file_post_response_id = create_file_post_response.id
                apiKey = verify_phone_post_response_api_key
                fileId = create_file_post_response_id
                callback.(client, apiKey, fileId)
            end


            def provide_api_key_and_folder_id(client, callback)
                register_phone_post_response = client.register_phone(
                    RegisterPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                    )
                )
                register_phone_post_response_code = register_phone_post_response.code
                verify_phone_post_response = client.verify_phone(
                    VerifyPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                        code = register_phone_post_response_code,
                        mcc = '300',
                        app = 'rec',
                        device_token = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_id = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_type = 'ios',
                        time_zone = '10',
                    )
                )
                verify_phone_post_response_api_key = verify_phone_post_response.api_key
                create_folder_post_response = client.create_folder(
                    CreateFolderRequest.new(
                        api_key = verify_phone_post_response_api_key,
                        name = 'test-folder',
                        pass = '12345',
                    )
                )
                create_folder_post_response_id = create_folder_post_response.id
                apiKey = verify_phone_post_response_api_key
                folderId = create_folder_post_response_id
                callback.(client, apiKey, folderId)
            end


            def provide_api_key_and_file_id_and_meta_file_id(client, callback)
                register_phone_post_response = client.register_phone(
                    RegisterPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                    )
                )
                register_phone_post_response_code = register_phone_post_response.code
                verify_phone_post_response = client.verify_phone(
                    VerifyPhoneRequest.new(
                        token = '55942ee3894f51000530894',
                        phone = '+16463742122',
                        code = register_phone_post_response_code,
                        mcc = '300',
                        app = 'rec',
                        device_token = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_id = '871284c348e04a9cacab8aca6b2f3c9a',
                        device_type = 'ios',
                        time_zone = '10',
                    )
                )
                verify_phone_post_response_api_key = verify_phone_post_response.api_key
                create_file_post_response = client.create_file(
                    CreateFileRequest.new(
                        api_key = verify_phone_post_response_api_key,
                        file = 'spec/resources/audio.mp3',
                        data = '',
                    )
                )
                create_file_post_response_id = create_file_post_response.id
                upload_meta_file_post_response = client.upload_meta_file(
                    UploadMetaFileRequest.new(
                        api_key = verify_phone_post_response_api_key,
                        file = 'spec/resources/java.png',
                        name = 'test-meta',
                        parent_id = create_file_post_response_id,
                        id = 1,
                    )
                )
                upload_meta_file_post_response_id = upload_meta_file_post_response.id
                apiKey = verify_phone_post_response_api_key
                fileId = create_file_post_response_id
                metaFileId = upload_meta_file_post_response_id
                callback.(client, apiKey, fileId, metaFileId)
            end


        end
    end

end
