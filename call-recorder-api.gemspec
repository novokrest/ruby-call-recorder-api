require 'rake'

Gem::Specification.new do |s|
    s.name = 'call-recorder-api'
    s.version = "1.2.0"
    s.date = %q{2020-04-11}
    s.summary = %q{Call Recorder API Client}
    s.authors = ["Konstantin Novokreshchenov"]
    s.files = `git ls-files -z lib/`.split("\0")

    s.add_dependency('rest-client', '~> 2.1')
  end