require 'call_recorder_api/client'

include CallRecorderApi

if __FILE__ == $0
    client = Client.new
    response = client.register_phone(
        RegisterPhoneRequest.new(
            token = '55942ee3894f51000530894',
            phone = '+16463742122',
        )
    )
    puts 'Registration code: ' + response.code
end
