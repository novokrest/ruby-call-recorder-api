module CallRecorderApi

    class Configuration

        attr_accessor :baseUrl

        def initialize(baseUrl = "https://app2.virtualbrix.net")
            @baseUrl = baseUrl
        end

    end

end
