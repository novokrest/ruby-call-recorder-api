module CallRecorderApi
    module Model

        class CreateFolderRequest

            attr_accessor :api_key
            attr_accessor :name
            attr_accessor :pass

            def initialize(
                api_key,
                name,
                pass
            )
                @api_key = api_key
                @name = name
                @pass = pass
            end

            def to_s
                'CreateFolderRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'pass=' + pass.to_s +
                '}'
            end

        end

    end
end
