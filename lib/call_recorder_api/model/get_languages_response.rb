module CallRecorderApi
    module Model

        class GetLanguagesResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :languages

            def initialize(
                status,
                msg,
                languages
            )
                @status = status
                @msg = msg
                @languages = languages
            end

            def to_s
                'GetLanguagesResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'languages=' + languages.to_s +
                '}'
            end

        end

    end
end
