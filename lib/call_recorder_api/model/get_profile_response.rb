module CallRecorderApi
    module Model

        class GetProfileResponse

            attr_accessor :status
            attr_accessor :code
            attr_accessor :profile
            attr_accessor :app
            attr_accessor :share_url
            attr_accessor :rate_url
            attr_accessor :credits
            attr_accessor :credits_trans

            def initialize(
                status,
                code,
                profile,
                app,
                share_url,
                rate_url,
                credits,
                credits_trans
            )
                @status = status
                @code = code
                @profile = profile
                @app = app
                @share_url = share_url
                @rate_url = rate_url
                @credits = credits
                @credits_trans = credits_trans
            end

            def to_s
                'GetProfileResponse{' +
                    'status=' + status.to_s + ', ' +
                    'code=' + code.to_s + ', ' +
                    'profile=' + profile.to_s + ', ' +
                    'app=' + app.to_s + ', ' +
                    'share_url=' + share_url.to_s + ', ' +
                    'rate_url=' + rate_url.to_s + ', ' +
                    'credits=' + credits.to_s + ', ' +
                    'credits_trans=' + credits_trans.to_s +
                '}'
            end

        end

    end
end
