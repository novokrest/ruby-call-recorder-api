module CallRecorderApi
    module Model

        class RegisterPhoneRequest

            attr_accessor :token
            attr_accessor :phone

            def initialize(
                token,
                phone
            )
                @token = token
                @phone = phone
            end

            def to_s
                'RegisterPhoneRequest{' +
                    'token=' + token.to_s + ', ' +
                    'phone=' + phone.to_s +
                '}'
            end

        end

    end
end
