module CallRecorderApi
    module Model

        class GetMetaFilesResponse

            attr_accessor :status
            attr_accessor :meta_files

            def initialize(
                status,
                meta_files
            )
                @status = status
                @meta_files = meta_files
            end

            def to_s
                'GetMetaFilesResponse{' +
                    'status=' + status.to_s + ', ' +
                    'meta_files=' + meta_files.to_s +
                '}'
            end

        end

    end
end
