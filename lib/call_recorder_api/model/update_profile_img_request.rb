module CallRecorderApi
    module Model

        class UpdateProfileImgRequest

            attr_accessor :api_key
            attr_accessor :file

            def initialize(
                api_key,
                file
            )
                @api_key = api_key
                @file = file
            end

            def to_s
                'UpdateProfileImgRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'file=' + file.to_s +
                '}'
            end

        end

    end
end
