module CallRecorderApi
    module Model

        class CreateFileData

            attr_accessor :name
            attr_accessor :email
            attr_accessor :phone
            attr_accessor :l_name
            attr_accessor :f_name
            attr_accessor :notes
            attr_accessor :tags
            attr_accessor :source
            attr_accessor :remind_days
            attr_accessor :remind_date

            def initialize(
                name,
                email,
                phone,
                l_name,
                f_name,
                notes,
                tags,
                source,
                remind_days,
                remind_date
            )
                @name = name
                @email = email
                @phone = phone
                @l_name = l_name
                @f_name = f_name
                @notes = notes
                @tags = tags
                @source = source
                @remind_days = remind_days
                @remind_date = remind_date
            end

            def to_s
                'CreateFileData{' +
                    'name=' + name.to_s + ', ' +
                    'email=' + email.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'l_name=' + l_name.to_s + ', ' +
                    'f_name=' + f_name.to_s + ', ' +
                    'notes=' + notes.to_s + ', ' +
                    'tags=' + tags.to_s + ', ' +
                    'source=' + source.to_s + ', ' +
                    'remind_days=' + remind_days.to_s + ', ' +
                    'remind_date=' + remind_date.to_s +
                '}'
            end

        end

    end
end
