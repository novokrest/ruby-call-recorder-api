module CallRecorderApi
    module Model

        class GetFoldersRequest

            attr_accessor :api_key

            def initialize(
                api_key
            )
                @api_key = api_key
            end

            def to_s
                'GetFoldersRequest{' +
                    'api_key=' + api_key.to_s +
                '}'
            end

        end

    end
end
