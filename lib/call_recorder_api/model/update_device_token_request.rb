module CallRecorderApi
    module Model

        class UpdateDeviceTokenRequest

            attr_accessor :api_key
            attr_accessor :device_token
            attr_accessor :device_type

            def initialize(
                api_key,
                device_token,
                device_type
            )
                @api_key = api_key
                @device_token = device_token
                @device_type = device_type
            end

            def to_s
                'UpdateDeviceTokenRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'device_token=' + device_token.to_s + ', ' +
                    'device_type=' + device_type.to_s +
                '}'
            end

        end

    end
end
