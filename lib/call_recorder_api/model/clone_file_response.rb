module CallRecorderApi
    module Model

        class CloneFileResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :code
            attr_accessor :id

            def initialize(
                status,
                msg,
                code,
                id
            )
                @status = status
                @msg = msg
                @code = code
                @id = id
            end

            def to_s
                'CloneFileResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'code=' + code.to_s + ', ' +
                    'id=' + id.to_s +
                '}'
            end

        end

    end
end
