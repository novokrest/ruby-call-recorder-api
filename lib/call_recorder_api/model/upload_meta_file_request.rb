module CallRecorderApi
    module Model

        class UploadMetaFileRequest

            attr_accessor :api_key
            attr_accessor :file
            attr_accessor :name
            attr_accessor :parent_id
            attr_accessor :id

            def initialize(
                api_key,
                file,
                name,
                parent_id,
                id
            )
                @api_key = api_key
                @file = file
                @name = name
                @parent_id = parent_id
                @id = id
            end

            def to_s
                'UploadMetaFileRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'file=' + file.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'parent_id=' + parent_id.to_s + ', ' +
                    'id=' + id.to_s +
                '}'
            end

        end

    end
end
