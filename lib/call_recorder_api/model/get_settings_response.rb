module CallRecorderApi
    module Model

        class GetSettingsResponse

            attr_accessor :status
            attr_accessor :app
            attr_accessor :credits
            attr_accessor :settings

            def initialize(
                status,
                app,
                credits,
                settings
            )
                @status = status
                @app = app
                @credits = credits
                @settings = settings
            end

            def to_s
                'GetSettingsResponse{' +
                    'status=' + status.to_s + ', ' +
                    'app=' + app.to_s + ', ' +
                    'credits=' + credits.to_s + ', ' +
                    'settings=' + settings.to_s +
                '}'
            end

        end

    end
end
