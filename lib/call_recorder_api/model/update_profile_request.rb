module CallRecorderApi
    module Model

        class UpdateProfileRequest

            attr_accessor :api_key
            attr_accessor :data

            def initialize(
                api_key,
                data
            )
                @api_key = api_key
                @data = data
            end

            def to_s
                'UpdateProfileRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'data=' + data.to_s +
                '}'
            end

        end

    end
end
