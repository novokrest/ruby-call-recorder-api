module CallRecorderApi
    module Model

        class GetMetaFilesResponseMetaFiles

            attr_accessor :id
            attr_accessor :parent_id
            attr_accessor :name
            attr_accessor :file
            attr_accessor :user_id
            attr_accessor :time

            def initialize(
                id,
                parent_id,
                name,
                file,
                user_id,
                time
            )
                @id = id
                @parent_id = parent_id
                @name = name
                @file = file
                @user_id = user_id
                @time = time
            end

            def to_s
                'GetMetaFilesResponseMetaFiles{' +
                    'id=' + id.to_s + ', ' +
                    'parent_id=' + parent_id.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'file=' + file.to_s + ', ' +
                    'user_id=' + user_id.to_s + ', ' +
                    'time=' + time.to_s +
                '}'
            end

        end

    end
end
