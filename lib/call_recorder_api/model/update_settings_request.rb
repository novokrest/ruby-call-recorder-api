module CallRecorderApi
    module Model

        class UpdateSettingsRequest

            attr_accessor :api_key
            attr_accessor :play_beep
            attr_accessor :files_permission

            def initialize(
                api_key,
                play_beep,
                files_permission
            )
                @api_key = api_key
                @play_beep = play_beep
                @files_permission = files_permission
            end

            def to_s
                'UpdateSettingsRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'play_beep=' + play_beep.to_s + ', ' +
                    'files_permission=' + files_permission.to_s +
                '}'
            end

        end

    end
end
