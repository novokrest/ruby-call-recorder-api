module CallRecorderApi
    module Model

        class VerifyPhoneRequest

            attr_accessor :token
            attr_accessor :phone
            attr_accessor :code
            attr_accessor :mcc
            attr_accessor :app
            attr_accessor :device_token
            attr_accessor :device_id
            attr_accessor :device_type
            attr_accessor :time_zone

            def initialize(
                token,
                phone,
                code,
                mcc,
                app,
                device_token,
                device_id,
                device_type,
                time_zone
            )
                @token = token
                @phone = phone
                @code = code
                @mcc = mcc
                @app = app
                @device_token = device_token
                @device_id = device_id
                @device_type = device_type
                @time_zone = time_zone
            end

            def to_s
                'VerifyPhoneRequest{' +
                    'token=' + token.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'code=' + code.to_s + ', ' +
                    'mcc=' + mcc.to_s + ', ' +
                    'app=' + app.to_s + ', ' +
                    'device_token=' + device_token.to_s + ', ' +
                    'device_id=' + device_id.to_s + ', ' +
                    'device_type=' + device_type.to_s + ', ' +
                    'time_zone=' + time_zone.to_s +
                '}'
            end

        end

    end
end
