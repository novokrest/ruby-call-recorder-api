module CallRecorderApi
    module Model

        class VerifyFolderPassRequest

            attr_accessor :api_key
            attr_accessor :id
            attr_accessor :pass

            def initialize(
                api_key,
                id,
                pass
            )
                @api_key = api_key
                @id = id
                @pass = pass
            end

            def to_s
                'VerifyFolderPassRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'pass=' + pass.to_s +
                '}'
            end

        end

    end
end
