module CallRecorderApi
    module Model

        class DeleteFolderRequest

            attr_accessor :api_key
            attr_accessor :id
            attr_accessor :move_to

            def initialize(
                api_key,
                id,
                move_to
            )
                @api_key = api_key
                @id = id
                @move_to = move_to
            end

            def to_s
                'DeleteFolderRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'move_to=' + move_to.to_s +
                '}'
            end

        end

    end
end
