module CallRecorderApi
    module Model

        class UpdateFileRequest

            attr_accessor :api_key
            attr_accessor :id
            attr_accessor :f_name
            attr_accessor :l_name
            attr_accessor :notes
            attr_accessor :email
            attr_accessor :phone
            attr_accessor :tags
            attr_accessor :folder_id
            attr_accessor :name
            attr_accessor :remind_days
            attr_accessor :remind_date

            def initialize(
                api_key,
                id,
                f_name,
                l_name,
                notes,
                email,
                phone,
                tags,
                folder_id,
                name,
                remind_days,
                remind_date
            )
                @api_key = api_key
                @id = id
                @f_name = f_name
                @l_name = l_name
                @notes = notes
                @email = email
                @phone = phone
                @tags = tags
                @folder_id = folder_id
                @name = name
                @remind_days = remind_days
                @remind_date = remind_date
            end

            def to_s
                'UpdateFileRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'f_name=' + f_name.to_s + ', ' +
                    'l_name=' + l_name.to_s + ', ' +
                    'notes=' + notes.to_s + ', ' +
                    'email=' + email.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'tags=' + tags.to_s + ', ' +
                    'folder_id=' + folder_id.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'remind_days=' + remind_days.to_s + ', ' +
                    'remind_date=' + remind_date.to_s +
                '}'
            end

        end

    end
end
