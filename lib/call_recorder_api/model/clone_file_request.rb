module CallRecorderApi
    module Model

        class CloneFileRequest

            attr_accessor :api_key
            attr_accessor :id

            def initialize(
                api_key,
                id
            )
                @api_key = api_key
                @id = id
            end

            def to_s
                'CloneFileRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s +
                '}'
            end

        end

    end
end
