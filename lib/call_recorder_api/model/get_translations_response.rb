module CallRecorderApi
    module Model

        class GetTranslationsResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :code
            attr_accessor :translation

            def initialize(
                status,
                msg,
                code,
                translation
            )
                @status = status
                @msg = msg
                @code = code
                @translation = translation
            end

            def to_s
                'GetTranslationsResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'code=' + code.to_s + ', ' +
                    'translation=' + translation.to_s +
                '}'
            end

        end

    end
end
