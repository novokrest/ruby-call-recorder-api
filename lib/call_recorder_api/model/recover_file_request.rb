module CallRecorderApi
    module Model

        class RecoverFileRequest

            attr_accessor :api_key
            attr_accessor :id
            attr_accessor :folder_id

            def initialize(
                api_key,
                id,
                folder_id
            )
                @api_key = api_key
                @id = id
                @folder_id = folder_id
            end

            def to_s
                'RecoverFileRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'folder_id=' + folder_id.to_s +
                '}'
            end

        end

    end
end
