module CallRecorderApi
    module Model

        class GetFoldersResponseFolder

            attr_accessor :id
            attr_accessor :name
            attr_accessor :created
            attr_accessor :updated
            attr_accessor :is_start
            attr_accessor :order_id

            def initialize(
                id,
                name,
                created,
                updated,
                is_start,
                order_id
            )
                @id = id
                @name = name
                @created = created
                @updated = updated
                @is_start = is_start
                @order_id = order_id
            end

            def to_s
                'GetFoldersResponseFolder{' +
                    'id=' + id.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'created=' + created.to_s + ', ' +
                    'updated=' + updated.to_s + ', ' +
                    'is_start=' + is_start.to_s + ', ' +
                    'order_id=' + order_id.to_s +
                '}'
            end

        end

    end
end
