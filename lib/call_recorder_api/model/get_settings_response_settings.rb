module CallRecorderApi
    module Model

        class GetSettingsResponseSettings

            attr_accessor :play_beep
            attr_accessor :files_permission

            def initialize(
                play_beep,
                files_permission
            )
                @play_beep = play_beep
                @files_permission = files_permission
            end

            def to_s
                'GetSettingsResponseSettings{' +
                    'play_beep=' + play_beep.to_s + ', ' +
                    'files_permission=' + files_permission.to_s +
                '}'
            end

        end

    end
end
