module CallRecorderApi
    module Model

        class GetFilesResponse

            attr_accessor :status
            attr_accessor :credits
            attr_accessor :credits_trans
            attr_accessor :files

            def initialize(
                status,
                credits,
                credits_trans,
                files
            )
                @status = status
                @credits = credits
                @credits_trans = credits_trans
                @files = files
            end

            def to_s
                'GetFilesResponse{' +
                    'status=' + status.to_s + ', ' +
                    'credits=' + credits.to_s + ', ' +
                    'credits_trans=' + credits_trans.to_s + ', ' +
                    'files=' + files.to_s +
                '}'
            end

        end

    end
end
