module CallRecorderApi
    module Model

        class UpdateUserRequest

            attr_accessor :api_key
            attr_accessor :app
            attr_accessor :time_zone

            def initialize(
                api_key,
                app,
                time_zone
            )
                @api_key = api_key
                @app = app
                @time_zone = time_zone
            end

            def to_s
                'UpdateUserRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'app=' + app.to_s + ', ' +
                    'time_zone=' + time_zone.to_s +
                '}'
            end

        end

    end
end
