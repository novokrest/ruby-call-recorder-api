module CallRecorderApi
    module Model

        class DeleteFilesRequest

            attr_accessor :api_key
            attr_accessor :ids
            attr_accessor :action

            def initialize(
                api_key,
                ids,
                action
            )
                @api_key = api_key
                @ids = ids
                @action = action
            end

            def to_s
                'DeleteFilesRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'ids=' + ids.to_s + ', ' +
                    'action=' + action.to_s +
                '}'
            end

        end

    end
end
