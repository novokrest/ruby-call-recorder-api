module CallRecorderApi
    module Model

        class UpdateUserResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :code

            def initialize(
                status,
                msg,
                code
            )
                @status = status
                @msg = msg
                @code = code
            end

            def to_s
                'UpdateUserResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'code=' + code.to_s +
                '}'
            end

        end

    end
end
