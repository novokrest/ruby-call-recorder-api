module CallRecorderApi
    module Model

        class CreateFileRequest

            attr_accessor :api_key
            attr_accessor :file
            attr_accessor :data

            def initialize(
                api_key,
                file,
                data
            )
                @api_key = api_key
                @file = file
                @data = data
            end

            def to_s
                'CreateFileRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'file=' + file.to_s + ', ' +
                    'data=' + data.to_s +
                '}'
            end

        end

    end
end
