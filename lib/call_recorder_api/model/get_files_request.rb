module CallRecorderApi
    module Model

        class GetFilesRequest

            attr_accessor :api_key
            attr_accessor :page
            attr_accessor :folder_id
            attr_accessor :source
            attr_accessor :pass
            attr_accessor :reminder
            attr_accessor :q
            attr_accessor :id
            attr_accessor :op

            def initialize(
                api_key,
                page,
                folder_id,
                source,
                pass,
                reminder,
                q,
                id,
                op
            )
                @api_key = api_key
                @page = page
                @folder_id = folder_id
                @source = source
                @pass = pass
                @reminder = reminder
                @q = q
                @id = id
                @op = op
            end

            def to_s
                'GetFilesRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'page=' + page.to_s + ', ' +
                    'folder_id=' + folder_id.to_s + ', ' +
                    'source=' + source.to_s + ', ' +
                    'pass=' + pass.to_s + ', ' +
                    'reminder=' + reminder.to_s + ', ' +
                    'q=' + q.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'op=' + op.to_s +
                '}'
            end

        end

    end
end
