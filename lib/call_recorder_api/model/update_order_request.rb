module CallRecorderApi
    module Model

        class UpdateOrderRequest

            attr_accessor :api_key
            attr_accessor :folders

            def initialize(
                api_key,
                folders
            )
                @api_key = api_key
                @folders = folders
            end

            def to_s
                'UpdateOrderRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'folders=' + folders.to_s +
                '}'
            end

        end

    end
end
