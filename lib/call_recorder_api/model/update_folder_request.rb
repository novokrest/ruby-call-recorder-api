module CallRecorderApi
    module Model

        class UpdateFolderRequest

            attr_accessor :api_key
            attr_accessor :id
            attr_accessor :name
            attr_accessor :pass
            attr_accessor :is_private

            def initialize(
                api_key,
                id,
                name,
                pass,
                is_private
            )
                @api_key = api_key
                @id = id
                @name = name
                @pass = pass
                @is_private = is_private
            end

            def to_s
                'UpdateFolderRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'pass=' + pass.to_s + ', ' +
                    'is_private=' + is_private.to_s +
                '}'
            end

        end

    end
end
