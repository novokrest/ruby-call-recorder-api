module CallRecorderApi
    module Model

        class UpdateDeviceTokenResponse

            attr_accessor :status
            attr_accessor :msg

            def initialize(
                status,
                msg
            )
                @status = status
                @msg = msg
            end

            def to_s
                'UpdateDeviceTokenResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s +
                '}'
            end

        end

    end
end
