module CallRecorderApi
    module Model

        class GetTranslationsRequest

            attr_accessor :api_key
            attr_accessor :language

            def initialize(
                api_key,
                language
            )
                @api_key = api_key
                @language = language
            end

            def to_s
                'GetTranslationsRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'language=' + language.to_s +
                '}'
            end

        end

    end
end
