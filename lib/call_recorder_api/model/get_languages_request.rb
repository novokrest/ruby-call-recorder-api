module CallRecorderApi
    module Model

        class GetLanguagesRequest

            attr_accessor :api_key

            def initialize(
                api_key
            )
                @api_key = api_key
            end

            def to_s
                'GetLanguagesRequest{' +
                    'api_key=' + api_key.to_s +
                '}'
            end

        end

    end
end
