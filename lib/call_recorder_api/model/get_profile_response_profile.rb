module CallRecorderApi
    module Model

        class GetProfileResponseProfile

            attr_accessor :f_name
            attr_accessor :l_name
            attr_accessor :email
            attr_accessor :phone
            attr_accessor :pic
            attr_accessor :language
            attr_accessor :is_public
            attr_accessor :play_beep
            attr_accessor :max_length
            attr_accessor :time_zone
            attr_accessor :time
            attr_accessor :pin

            def initialize(
                f_name,
                l_name,
                email,
                phone,
                pic,
                language,
                is_public,
                play_beep,
                max_length,
                time_zone,
                time,
                pin
            )
                @f_name = f_name
                @l_name = l_name
                @email = email
                @phone = phone
                @pic = pic
                @language = language
                @is_public = is_public
                @play_beep = play_beep
                @max_length = max_length
                @time_zone = time_zone
                @time = time
                @pin = pin
            end

            def to_s
                'GetProfileResponseProfile{' +
                    'f_name=' + f_name.to_s + ', ' +
                    'l_name=' + l_name.to_s + ', ' +
                    'email=' + email.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'pic=' + pic.to_s + ', ' +
                    'language=' + language.to_s + ', ' +
                    'is_public=' + is_public.to_s + ', ' +
                    'play_beep=' + play_beep.to_s + ', ' +
                    'max_length=' + max_length.to_s + ', ' +
                    'time_zone=' + time_zone.to_s + ', ' +
                    'time=' + time.to_s + ', ' +
                    'pin=' + pin.to_s +
                '}'
            end

        end

    end
end
