module CallRecorderApi
    module Model

        class UploadMetaFileResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :parent_id
            attr_accessor :id

            def initialize(
                status,
                msg,
                parent_id,
                id
            )
                @status = status
                @msg = msg
                @parent_id = parent_id
                @id = id
            end

            def to_s
                'UploadMetaFileResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'parent_id=' + parent_id.to_s + ', ' +
                    'id=' + id.to_s +
                '}'
            end

        end

    end
end
