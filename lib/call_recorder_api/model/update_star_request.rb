module CallRecorderApi
    module Model

        class UpdateStarRequest

            attr_accessor :api_key
            attr_accessor :id
            attr_accessor :star
            attr_accessor :type

            def initialize(
                api_key,
                id,
                star,
                type
            )
                @api_key = api_key
                @id = id
                @star = star
                @type = type
            end

            def to_s
                'UpdateStarRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'star=' + star.to_s + ', ' +
                    'type=' + type.to_s +
                '}'
            end

        end

    end
end
