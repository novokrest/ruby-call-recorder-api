module CallRecorderApi
    module Model

        class GetMetaFilesRequest

            attr_accessor :api_key
            attr_accessor :parent_id

            def initialize(
                api_key,
                parent_id
            )
                @api_key = api_key
                @parent_id = parent_id
            end

            def to_s
                'GetMetaFilesRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'parent_id=' + parent_id.to_s +
                '}'
            end

        end

    end
end
