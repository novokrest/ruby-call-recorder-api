module CallRecorderApi
    module Model

        class UpdateProfileRequestData

            attr_accessor :f_name
            attr_accessor :l_name
            attr_accessor :email
            attr_accessor :is_public
            attr_accessor :language

            def initialize(
                f_name,
                l_name,
                email,
                is_public,
                language
            )
                @f_name = f_name
                @l_name = l_name
                @email = email
                @is_public = is_public
                @language = language
            end

            def to_s
                'UpdateProfileRequestData{' +
                    'f_name=' + f_name.to_s + ', ' +
                    'l_name=' + l_name.to_s + ', ' +
                    'email=' + email.to_s + ', ' +
                    'is_public=' + is_public.to_s + ', ' +
                    'language=' + language.to_s +
                '}'
            end

        end

    end
end
