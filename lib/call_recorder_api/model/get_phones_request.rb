module CallRecorderApi
    module Model

        class GetPhonesRequest

            attr_accessor :api_key

            def initialize(
                api_key
            )
                @api_key = api_key
            end

            def to_s
                'GetPhonesRequest{' +
                    'api_key=' + api_key.to_s +
                '}'
            end

        end

    end
end
