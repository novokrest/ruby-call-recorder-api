module CallRecorderApi
    module Model

        class VerifyPhoneResponse

            attr_accessor :status
            attr_accessor :phone
            attr_accessor :api_key
            attr_accessor :msg

            def initialize(
                status,
                phone,
                api_key,
                msg
            )
                @status = status
                @phone = phone
                @api_key = api_key
                @msg = msg
            end

            def to_s
                'VerifyPhoneResponse{' +
                    'status=' + status.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'api_key=' + api_key.to_s + ', ' +
                    'msg=' + msg.to_s +
                '}'
            end

        end

    end
end
