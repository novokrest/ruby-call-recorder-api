module CallRecorderApi
    module Model

        class GetMessagesResponse

            attr_accessor :status
            attr_accessor :msgs

            def initialize(
                status,
                msgs
            )
                @status = status
                @msgs = msgs
            end

            def to_s
                'GetMessagesResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msgs=' + msgs.to_s +
                '}'
            end

        end

    end
end
