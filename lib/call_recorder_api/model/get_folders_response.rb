module CallRecorderApi
    module Model

        class GetFoldersResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :folders

            def initialize(
                status,
                msg,
                folders
            )
                @status = status
                @msg = msg
                @folders = folders
            end

            def to_s
                'GetFoldersResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'folders=' + folders.to_s +
                '}'
            end

        end

    end
end
