module CallRecorderApi
    module Model

        class BuyCreditsRequest

            attr_accessor :api_key
            attr_accessor :amount
            attr_accessor :receipt
            attr_accessor :product_id
            attr_accessor :device_type

            def initialize(
                api_key,
                amount,
                receipt,
                product_id,
                device_type
            )
                @api_key = api_key
                @amount = amount
                @receipt = receipt
                @product_id = product_id
                @device_type = device_type
            end

            def to_s
                'BuyCreditsRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'amount=' + amount.to_s + ', ' +
                    'receipt=' + receipt.to_s + ', ' +
                    'product_id=' + product_id.to_s + ', ' +
                    'device_type=' + device_type.to_s +
                '}'
            end

        end

    end
end
