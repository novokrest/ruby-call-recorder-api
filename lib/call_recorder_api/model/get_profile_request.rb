module CallRecorderApi
    module Model

        class GetProfileRequest

            attr_accessor :api_key

            def initialize(
                api_key
            )
                @api_key = api_key
            end

            def to_s
                'GetProfileRequest{' +
                    'api_key=' + api_key.to_s +
                '}'
            end

        end

    end
end
