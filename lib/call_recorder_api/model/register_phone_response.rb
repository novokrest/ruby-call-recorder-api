module CallRecorderApi
    module Model

        class RegisterPhoneResponse

            attr_accessor :status
            attr_accessor :phone
            attr_accessor :code
            attr_accessor :msg

            def initialize(
                status,
                phone,
                code,
                msg
            )
                @status = status
                @phone = phone
                @code = code
                @msg = msg
            end

            def to_s
                'RegisterPhoneResponse{' +
                    'status=' + status.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'code=' + code.to_s + ', ' +
                    'msg=' + msg.to_s +
                '}'
            end

        end

    end
end
