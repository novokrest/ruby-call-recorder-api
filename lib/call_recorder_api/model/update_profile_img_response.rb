module CallRecorderApi
    module Model

        class UpdateProfileImgResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :code
            attr_accessor :file
            attr_accessor :path

            def initialize(
                status,
                msg,
                code,
                file,
                path
            )
                @status = status
                @msg = msg
                @code = code
                @file = file
                @path = path
            end

            def to_s
                'UpdateProfileImgResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'code=' + code.to_s + ', ' +
                    'file=' + file.to_s + ', ' +
                    'path=' + path.to_s +
                '}'
            end

        end

    end
end
