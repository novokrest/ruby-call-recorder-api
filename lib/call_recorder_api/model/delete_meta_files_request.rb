module CallRecorderApi
    module Model

        class DeleteMetaFilesRequest

            attr_accessor :api_key
            attr_accessor :ids
            attr_accessor :parent_id

            def initialize(
                api_key,
                ids,
                parent_id
            )
                @api_key = api_key
                @ids = ids
                @parent_id = parent_id
            end

            def to_s
                'DeleteMetaFilesRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'ids=' + ids.to_s + ', ' +
                    'parent_id=' + parent_id.to_s +
                '}'
            end

        end

    end
end
