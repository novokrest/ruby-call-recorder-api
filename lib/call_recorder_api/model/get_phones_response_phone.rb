module CallRecorderApi
    module Model

        class GetPhonesResponsePhone

            attr_accessor :phone_number
            attr_accessor :number
            attr_accessor :prefix
            attr_accessor :friendly_name
            attr_accessor :flag
            attr_accessor :city
            attr_accessor :country

            def initialize(
                phone_number,
                number,
                prefix,
                friendly_name,
                flag,
                city,
                country
            )
                @phone_number = phone_number
                @number = number
                @prefix = prefix
                @friendly_name = friendly_name
                @flag = flag
                @city = city
                @country = country
            end

            def to_s
                'GetPhonesResponsePhone{' +
                    'phone_number=' + phone_number.to_s + ', ' +
                    'number=' + number.to_s + ', ' +
                    'prefix=' + prefix.to_s + ', ' +
                    'friendly_name=' + friendly_name.to_s + ', ' +
                    'flag=' + flag.to_s + ', ' +
                    'city=' + city.to_s + ', ' +
                    'country=' + country.to_s +
                '}'
            end

        end

    end
end
