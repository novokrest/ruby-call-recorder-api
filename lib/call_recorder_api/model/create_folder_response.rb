module CallRecorderApi
    module Model

        class CreateFolderResponse

            attr_accessor :status
            attr_accessor :msg
            attr_accessor :id
            attr_accessor :code

            def initialize(
                status,
                msg,
                id,
                code
            )
                @status = status
                @msg = msg
                @id = id
                @code = code
            end

            def to_s
                'CreateFolderResponse{' +
                    'status=' + status.to_s + ', ' +
                    'msg=' + msg.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'code=' + code.to_s +
                '}'
            end

        end

    end
end
