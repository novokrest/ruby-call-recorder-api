module CallRecorderApi
    module Model

        class GetFilesResponseFile

            attr_accessor :id
            attr_accessor :access_number
            attr_accessor :name
            attr_accessor :f_name
            attr_accessor :l_name
            attr_accessor :email
            attr_accessor :phone
            attr_accessor :notes
            attr_accessor :meta
            attr_accessor :source
            attr_accessor :url
            attr_accessor :credits
            attr_accessor :duration
            attr_accessor :time
            attr_accessor :share_url
            attr_accessor :download_url

            def initialize(
                id,
                access_number,
                name,
                f_name,
                l_name,
                email,
                phone,
                notes,
                meta,
                source,
                url,
                credits,
                duration,
                time,
                share_url,
                download_url
            )
                @id = id
                @access_number = access_number
                @name = name
                @f_name = f_name
                @l_name = l_name
                @email = email
                @phone = phone
                @notes = notes
                @meta = meta
                @source = source
                @url = url
                @credits = credits
                @duration = duration
                @time = time
                @share_url = share_url
                @download_url = download_url
            end

            def to_s
                'GetFilesResponseFile{' +
                    'id=' + id.to_s + ', ' +
                    'access_number=' + access_number.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'f_name=' + f_name.to_s + ', ' +
                    'l_name=' + l_name.to_s + ', ' +
                    'email=' + email.to_s + ', ' +
                    'phone=' + phone.to_s + ', ' +
                    'notes=' + notes.to_s + ', ' +
                    'meta=' + meta.to_s + ', ' +
                    'source=' + source.to_s + ', ' +
                    'url=' + url.to_s + ', ' +
                    'credits=' + credits.to_s + ', ' +
                    'duration=' + duration.to_s + ', ' +
                    'time=' + time.to_s + ', ' +
                    'share_url=' + share_url.to_s + ', ' +
                    'download_url=' + download_url.to_s +
                '}'
            end

        end

    end
end
