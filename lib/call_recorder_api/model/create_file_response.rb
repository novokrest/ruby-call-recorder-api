module CallRecorderApi
    module Model

        class CreateFileResponse

            attr_accessor :status
            attr_accessor :id
            attr_accessor :msg

            def initialize(
                status,
                id,
                msg
            )
                @status = status
                @id = id
                @msg = msg
            end

            def to_s
                'CreateFileResponse{' +
                    'status=' + status.to_s + ', ' +
                    'id=' + id.to_s + ', ' +
                    'msg=' + msg.to_s +
                '}'
            end

        end

    end
end
