module CallRecorderApi
    module Model

        class Language

            attr_accessor :code
            attr_accessor :name
            attr_accessor :flag

            def initialize(
                code,
                name,
                flag
            )
                @code = code
                @name = name
                @flag = flag
            end

            def to_s
                'Language{' +
                    'code=' + code.to_s + ', ' +
                    'name=' + name.to_s + ', ' +
                    'flag=' + flag.to_s +
                '}'
            end

        end

    end
end
