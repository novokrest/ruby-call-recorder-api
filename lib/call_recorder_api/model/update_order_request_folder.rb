module CallRecorderApi
    module Model

        class UpdateOrderRequestFolder

            attr_accessor :id
            attr_accessor :order_id

            def initialize(
                id,
                order_id
            )
                @id = id
                @order_id = order_id
            end

            def to_s
                'UpdateOrderRequestFolder{' +
                    'id=' + id.to_s + ', ' +
                    'order_id=' + order_id.to_s +
                '}'
            end

        end

    end
end
