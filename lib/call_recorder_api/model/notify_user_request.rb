module CallRecorderApi
    module Model

        class NotifyUserRequest

            attr_accessor :api_key
            attr_accessor :title
            attr_accessor :body
            attr_accessor :device_type

            def initialize(
                api_key,
                title,
                body,
                device_type
            )
                @api_key = api_key
                @title = title
                @body = body
                @device_type = device_type
            end

            def to_s
                'NotifyUserRequest{' +
                    'api_key=' + api_key.to_s + ', ' +
                    'title=' + title.to_s + ', ' +
                    'body=' + body.to_s + ', ' +
                    'device_type=' + device_type.to_s +
                '}'
            end

        end

    end
end
