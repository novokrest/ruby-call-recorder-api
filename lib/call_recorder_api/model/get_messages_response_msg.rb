module CallRecorderApi
    module Model

        class GetMessagesResponseMsg

            attr_accessor :id
            attr_accessor :title
            attr_accessor :body
            attr_accessor :time

            def initialize(
                id,
                title,
                body,
                time
            )
                @id = id
                @title = title
                @body = body
                @time = time
            end

            def to_s
                'GetMessagesResponseMsg{' +
                    'id=' + id.to_s + ', ' +
                    'title=' + title.to_s + ', ' +
                    'body=' + body.to_s + ', ' +
                    'time=' + time.to_s +
                '}'
            end

        end

    end
end
