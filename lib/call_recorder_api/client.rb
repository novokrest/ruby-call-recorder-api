require 'call_recorder_api/client/configuration'
require 'call_recorder_api/model'
require 'rest-client'
require 'json'

module CallRecorderApi
    class Client
        def initialize(config = Configuration.new)
            @config = config
        end

        def buy_credits(request)
            r = RestClient.post(@config.baseUrl + '/rapi/buy_credits', {
                'amount': request.amount,
                'api_key': request.api_key,
                'device_type': request.device_type,
                'product_id': request.product_id,
                'receipt': request.receipt,
            })
            json = JSON.parse(r)
            BuyCreditsResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def clone_file(request)
            r = RestClient.post(@config.baseUrl + '/rapi/clone_file', {
                'api_key': request.api_key,
                'id': request.id,
            })
            json = JSON.parse(r)
            CloneFileResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
                id = json['id'],
            )
        end

        def create_file(request)
            r = RestClient.post(@config.baseUrl + '/rapi/create_file', {
                'api_key': request.api_key,
                'data': request.data.to_json,
                'file': File.new(request.file, 'rb'),
            })
            json = JSON.parse(r)
            CreateFileResponse.new(
                status = json['status'],
                id = json['id'],
                msg = json['msg'],
            )
        end

        def create_folder(request)
            r = RestClient.post(@config.baseUrl + '/rapi/create_folder', {
                'api_key': request.api_key,
                'name': request.name,
                'pass': request.pass,
            })
            json = JSON.parse(r)
            CreateFolderResponse.new(
                status = json['status'],
                msg = json['msg'],
                id = json['id'],
                code = json['code'],
            )
        end

        def delete_files(request)
            r = RestClient.post(@config.baseUrl + '/rapi/delete_files', {
                'action': request.action,
                'api_key': request.api_key,
                'ids': request.ids.join(','),
            })
            json = JSON.parse(r)
            DeleteFilesResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def delete_folder(request)
            r = RestClient.post(@config.baseUrl + '/rapi/delete_folder', {
                'api_key': request.api_key,
                'id': request.id,
                'move_to': request.move_to,
            })
            json = JSON.parse(r)
            DeleteFolderResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def delete_meta_files(request)
            r = RestClient.post(@config.baseUrl + '/rapi/delete_meta_files', {
                'api_key': request.api_key,
                'ids': request.ids.join(','),
                'parent_id': request.parent_id,
            })
            json = JSON.parse(r)
            DeleteMetaFilesResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def get_files(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_files', {
                'api_key': request.api_key,
                'folder_id': request.folder_id,
                'id': request.id,
                'op': request.op,
                'page': request.page,
                'pass': request.pass,
                'q': request.q,
                'reminder': request.reminder,
                'source': request.source,
            })
            json = JSON.parse(r)
            GetFilesResponse.new(
                status = json['status'],
                credits = json['credits'],
                credits_trans = json['credits_trans'],
                files = json['files'],
            )
        end

        def get_folders(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_folders', {
                'api_key': request.api_key,
            })
            json = JSON.parse(r)
            GetFoldersResponse.new(
                status = json['status'],
                msg = json['msg'],
                folders = json['folders'],
            )
        end

        def get_languages(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_languages', {
                'api_key': request.api_key,
            })
            json = JSON.parse(r)
            GetLanguagesResponse.new(
                status = json['status'],
                msg = json['msg'],
                languages = json['languages'],
            )
        end

        def get_meta_files(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_meta_files', {
                'api_key': request.api_key,
                'parent_id': request.parent_id,
            })
            json = JSON.parse(r)
            GetMetaFilesResponse.new(
                status = json['status'],
                meta_files = json['meta_files'],
            )
        end

        def get_msgs(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_msgs', {
                'api_key': request.api_key,
            })
            json = JSON.parse(r)
            GetMessagesResponse.new(
                status = json['status'],
                msgs = json['msgs'],
            )
        end

        def get_phones(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_phones', {
                'api_key': request.api_key,
            })
            json = JSON.parse(r)
            GetPhonesResponse.new(
            )
        end

        def get_profile(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_profile', {
                'api_key': request.api_key,
            })
            json = JSON.parse(r)
            GetProfileResponse.new(
                status = json['status'],
                code = json['code'],
                profile = json['profile'],
                app = json['app'],
                share_url = json['share_url'],
                rate_url = json['rate_url'],
                credits = json['credits'],
                credits_trans = json['credits_trans'],
            )
        end

        def get_settings(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_settings', {
                'api_key': request.api_key,
            })
            json = JSON.parse(r)
            GetSettingsResponse.new(
                status = json['status'],
                app = json['app'],
                credits = json['credits'],
                settings = json['settings'],
            )
        end

        def get_translations(request)
            r = RestClient.post(@config.baseUrl + '/rapi/get_translations', {
                'api_key': request.api_key,
                'language': request.language,
            })
            json = JSON.parse(r)
            GetTranslationsResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
                translation = json['translation'],
            )
        end

        def notify_user_custom(request)
            r = RestClient.post(@config.baseUrl + '/rapi/notify_user_custom', {
                'api_key': request.api_key,
                'body': request.body,
                'device_type': request.device_type,
                'title': request.title,
            })
            json = JSON.parse(r)
            NotifyUserResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def recover_file(request)
            r = RestClient.post(@config.baseUrl + '/rapi/recover_file', {
                'api_key': request.api_key,
                'folder_id': request.folder_id,
                'id': request.id,
            })
            json = JSON.parse(r)
            RecoverFileResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def register_phone(request)
            r = RestClient.post(@config.baseUrl + '/rapi/register_phone', {
                'phone': request.phone,
                'token': request.token,
            })
            json = JSON.parse(r)
            RegisterPhoneResponse.new(
                status = json['status'],
                phone = json['phone'],
                code = json['code'],
                msg = json['msg'],
            )
        end

        def update_device_token(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_device_token', {
                'api_key': request.api_key,
                'device_token': request.device_token,
                'device_type': request.device_type,
            })
            json = JSON.parse(r)
            UpdateDeviceTokenResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def update_file(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_file', {
                'api_key': request.api_key,
                'email': request.email,
                'f_name': request.f_name,
                'folder_id': request.folder_id,
                'id': request.id,
                'l_name': request.l_name,
                'name': request.name,
                'notes': request.notes,
                'phone': request.phone,
                'remind_date': request.remind_date,
                'remind_days': request.remind_days,
                'tags': request.tags,
            })
            json = JSON.parse(r)
            UpdateFileResponse.new(
                status = json['status'],
                msg = json['msg'],
            )
        end

        def update_folder(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_folder', {
                'api_key': request.api_key,
                'id': request.id,
                'is_private': request.is_private,
                'name': request.name,
                'pass': request.pass,
            })
            json = JSON.parse(r)
            UpdateFolderResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def update_order(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_order', {
                'api_key': request.api_key,
                'folders': request.folders.join(','),
            })
            json = JSON.parse(r)
            UpdateOrderResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def update_profile_img(request)
            r = RestClient.post(@config.baseUrl + '/upload/update_profile_img', {
                'api_key': request.api_key,
                'file': File.new(request.file, 'rb'),
            })
            json = JSON.parse(r)
            UpdateProfileImgResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
                file = json['file'],
                path = json['path'],
            )
        end

        def update_profile(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_profile', {
                'api_key': request.api_key,
                'data[f_name]': request.data.f_name,
                'data[l_name]': request.data.l_name,
                'data[email]': request.data.email,
                'data[is_public]': request.data.is_public,
                'data[language]': request.data.language,
            })
            json = JSON.parse(r)
            UpdateProfileResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def update_settings(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_settings', {
                'api_key': request.api_key,
                'files_permission': request.files_permission,
                'play_beep': request.play_beep,
            })
            json = JSON.parse(r)
            UpdateSettingsResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def update_star(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_star', {
                'api_key': request.api_key,
                'id': request.id,
                'star': request.star,
                'type': request.type,
            })
            json = JSON.parse(r)
            UpdateStarResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def update_user(request)
            r = RestClient.post(@config.baseUrl + '/rapi/update_user', {
                'api_key': request.api_key,
                'app': request.app,
                'time_zone': request.time_zone,
            })
            json = JSON.parse(r)
            UpdateUserResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def upload_meta_file(request)
            r = RestClient.post(@config.baseUrl + '/rapi/upload_meta_file', {
                'api_key': request.api_key,
                'file': File.new(request.file, 'rb'),
                'id': request.id,
                'name': request.name,
                'parent_id': request.parent_id,
            })
            json = JSON.parse(r)
            UploadMetaFileResponse.new(
                status = json['status'],
                msg = json['msg'],
                parent_id = json['parent_id'],
                id = json['id'],
            )
        end

        def verify_folder_pass(request)
            r = RestClient.post(@config.baseUrl + '/rapi/verify_folder_pass', {
                'api_key': request.api_key,
                'id': request.id,
                'pass': request.pass,
            })
            json = JSON.parse(r)
            VerifyFolderPassResponse.new(
                status = json['status'],
                msg = json['msg'],
                code = json['code'],
            )
        end

        def verify_phone(request)
            r = RestClient.post(@config.baseUrl + '/rapi/verify_phone', {
                'app': request.app,
                'code': request.code,
                'device_id': request.device_id,
                'device_token': request.device_token,
                'device_type': request.device_type,
                'mcc': request.mcc,
                'phone': request.phone,
                'time_zone': request.time_zone,
                'token': request.token,
            })
            json = JSON.parse(r)
            VerifyPhoneResponse.new(
                status = json['status'],
                phone = json['phone'],
                api_key = json['api_key'],
                msg = json['msg'],
            )
        end

    end
end
