# ruby-call-recorder-api

## Environment

```bash
rbenv init
rbenv local
bundle install
```

## Test
```bash
bin/rspec spec --tag '~skip'
```

```bash
./bin/rspec spec/call_recorder_api/client_spec.rb:8
```

## Publish
```bash
gem build call-recorder-api.gemspec
gem push call-recorder-api-<version>.gem
```
